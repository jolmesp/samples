using apps.Context;
using apps.Models;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace apps.Pages.Admin
{
    public class TmpModel : PageModel
    {
        private readonly ConferenceContext _context;

        public TmpModel(ConferenceContext context)
        {
            _context = context;
        }

        public IList<Bootcamp> Bootcamp { get; set; }
        public IList<Exhibitor> Exhibitor { get; set; }
        public IList<Expo> Expo { get; set; }
        public IList<Speaker> Speaker { get; set; }

        public async Task OnGetAsync()
        {
            Bootcamp = await _context.Bootcamps.OrderByDescending(x => x.Id).ToListAsync();
            Exhibitor = await _context.Exhibitors.OrderByDescending(i => i.Id).ToListAsync();
            Expo = await _context.Expos.OrderByDescending(i => i.Id).ToListAsync();
            Speaker = await _context.Speakers.OrderByDescending(i => i.Id).ToListAsync();


            var bootcampCount = Bootcamp.Count;
            var exhibitorCount = Exhibitor.Count;
            var expoCount = Expo.Count;
            var speakerCount = Speaker.Count;
            
        }

        public IActionResult OnPostBootcampExcel()
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Bootcamp");
                var headers = worksheet.Range("A1:R1");
                headers.Style.Font.Bold = true;
                headers.Style.Fill.BackgroundColor = XLColor.Aqua;
                worksheet.Rows(1, 2).Style.Alignment.WrapText = true; 

                worksheet.Rows(1,2).AdjustToContents();
                worksheet.Columns(1, 18).AdjustToContents();
                var currentRow = 1;
                
                worksheet.Cell(currentRow, 1).Value = "ROWID";
                worksheet.Cell(currentRow, 2).Value = "NPI_NUMBER";
                worksheet.Cell(currentRow, 3).Value = "FIRST_NAME";
                worksheet.Cell(currentRow, 4).Value = "LAST_NAME";
                worksheet.Cell(currentRow, 5).Value = "BADGE";
                worksheet.Cell(currentRow, 6).Value = "TITLE";
                worksheet.Cell(currentRow, 7).Value = "ORGANIZATION";
                worksheet.Cell(currentRow, 8).Value = "ORGANIZATION_SIZE";
                worksheet.Cell(currentRow, 9).Value = "ADDRESS";
                worksheet.Cell(currentRow, 10).Value = "CITY";
                worksheet.Cell(currentRow, 11).Value = "PROVINCE";
                worksheet.Cell(currentRow, 12).Value = "POSTAL_CODE";
                worksheet.Cell(currentRow, 13).Value = "EMAIL";
                worksheet.Cell(currentRow, 14).Value = "EMERG_CONTACT_NAME";
                worksheet.Cell(currentRow, 15).Value = "EMERG_CONTACT_PHONE";
                worksheet.Cell(currentRow, 16).Value = "ACCESSIBILITY";
                worksheet.Cell(currentRow, 17).Value = "FIRST_NPI_CONFERENCE";
                worksheet.Cell(currentRow, 18).Value = "EXCLUDE_FROM_ATTENDEE_LIST"; 



                foreach (var bootcamp in _context.Bootcamps.ToList())
                {
                    currentRow++;
                    worksheet.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(currentRow, 1).Value =  bootcamp.Id;
                    worksheet.Cell(currentRow, 2).Value =  bootcamp.NpiNumber;
                    worksheet.Cell(currentRow, 3).Value =  bootcamp.FirstName;
                    worksheet.Cell(currentRow, 4).Value =  bootcamp.LastName;
                    worksheet.Cell(currentRow, 5).Value =  bootcamp.BadgeName;
                    worksheet.Cell(currentRow, 6).Value =  bootcamp.Title;
                    worksheet.Cell(currentRow, 7).Value =  bootcamp.Organization;
                    worksheet.Cell(currentRow, 8).Value =  bootcamp.NumberOfEmployees;
                    worksheet.Cell(currentRow, 9).Value =  bootcamp.Address; 
                    worksheet.Cell(currentRow, 10).Value = bootcamp.City;
                    worksheet.Cell(currentRow, 11).Value = bootcamp.Province;
                    worksheet.Cell(currentRow, 12).Value = bootcamp.PostalCode;
                    worksheet.Cell(currentRow, 13).Value = bootcamp.Email;
                    worksheet.Cell(currentRow, 14).Value = bootcamp.EmergencyContactName;
                    worksheet.Cell(currentRow, 15).Value = bootcamp.EmergencyContactPhoneNumber;
                    worksheet.Cell(currentRow, 16).Value = bootcamp.AccessibilityRequirements;
                    worksheet.Cell(currentRow, 17).Value = bootcamp.FirstNPIConference;
                    worksheet.Cell(currentRow, 18).Value = bootcamp.Distribute;
                }

                using (var stream = new MemoryStream())
                {

                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "BootcampRegistrations.xlsx");
                }
            }
        } 


        public IActionResult OnPostExhibitorExcel()
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Exhibitors");
                var headers = worksheet.Range("A1:R1");
                headers.Style.Font.Bold = true;
                headers.Style.Fill.BackgroundColor = XLColor.Aqua;
                worksheet.Rows(1, 2).Style.Alignment.WrapText = true;

                worksheet.Rows(1, 2).AdjustToContents();
                worksheet.Columns(1, 18).AdjustToContents();
                var currentRow = 1;

                worksheet.Cell(currentRow, 1).Value = "ROWID";
                worksheet.Cell(currentRow, 2).Value = "NPI_NUMBER";
                worksheet.Cell(currentRow, 3).Value = "FIRST_NAME";
                worksheet.Cell(currentRow, 4).Value = "LAST_NAME";
                worksheet.Cell(currentRow, 5).Value = "BADGE";
                worksheet.Cell(currentRow, 6).Value = "TITLE";
                worksheet.Cell(currentRow, 7).Value = "ORGANIZATION";
                worksheet.Cell(currentRow, 8).Value = "ORGANIZATION_SIZE";
                worksheet.Cell(currentRow, 9).Value = "ADDRESS";
                worksheet.Cell(currentRow, 10).Value = "CITY";
                worksheet.Cell(currentRow, 11).Value = "PROVINCE";
                worksheet.Cell(currentRow, 12).Value = "POSTAL_CODE";
                worksheet.Cell(currentRow, 13).Value = "EMAIL";
                worksheet.Cell(currentRow, 14).Value = "EMERG_CONTACT_NAME";
                worksheet.Cell(currentRow, 15).Value = "EMERG_CONTACT_PHONE";
                worksheet.Cell(currentRow, 16).Value = "ACCESSIBILITY";
                worksheet.Cell(currentRow, 17).Value = "FIRST_NPI_CONFERENCE";
                worksheet.Cell(currentRow, 18).Value = "EXCLUDE_FROM_ATTENDEE_LIST";



                foreach (var exhibitor in _context.Exhibitors.ToList())
                {
                    currentRow++;
                    worksheet.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(currentRow, 1).Value = exhibitor.Id;
                    worksheet.Cell(currentRow, 2).Value = exhibitor.NpiNumber;
                    worksheet.Cell(currentRow, 3).Value = exhibitor.FirstName;
                    worksheet.Cell(currentRow, 4).Value = exhibitor.LastName;
                    worksheet.Cell(currentRow, 5).Value = exhibitor.BadgeName;
                    worksheet.Cell(currentRow, 6).Value = exhibitor.Title;
                    worksheet.Cell(currentRow, 7).Value = exhibitor.Organization;
                    worksheet.Cell(currentRow, 8).Value = exhibitor.NumberOfEmployees;
                    worksheet.Cell(currentRow, 9).Value = exhibitor.Address;
                    worksheet.Cell(currentRow, 10).Value = exhibitor.City;
                    worksheet.Cell(currentRow, 11).Value = exhibitor.Province;
                    worksheet.Cell(currentRow, 12).Value = exhibitor.PostalCode;
                    worksheet.Cell(currentRow, 13).Value = exhibitor.Email;
                    worksheet.Cell(currentRow, 14).Value = exhibitor.EmergencyContactName;
                    worksheet.Cell(currentRow, 15).Value = exhibitor.EmergencyContactPhoneNumber;
                    worksheet.Cell(currentRow, 16).Value = exhibitor.AccessibilityRequirements;
                    worksheet.Cell(currentRow, 17).Value = exhibitor.FirstNPIConference;
                    worksheet.Cell(currentRow, 18).Value = exhibitor.Distribute;
                }

                using (var stream = new MemoryStream())
                {

                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "ExhibitorsRegistrations.xlsx");
                }
            }
        }

        public IActionResult OnPostExpoExcel()
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Expo");
                var headers = worksheet.Range("A1:R1");
                headers.Style.Font.Bold = true;
                headers.Style.Fill.BackgroundColor = XLColor.Aqua;
                worksheet.Rows(1, 2).Style.Alignment.WrapText = true;

                worksheet.Rows(1, 2).AdjustToContents();
                worksheet.Columns(1, 18).AdjustToContents();
                var currentRow = 1;

                worksheet.Cell(currentRow, 1).Value = "ROWID";
                worksheet.Cell(currentRow, 2).Value = "NPI_NUMBER";
                worksheet.Cell(currentRow, 3).Value = "FIRST_NAME";
                worksheet.Cell(currentRow, 4).Value = "LAST_NAME";
                worksheet.Cell(currentRow, 5).Value = "BADGE";
                worksheet.Cell(currentRow, 6).Value = "TITLE";
                worksheet.Cell(currentRow, 7).Value = "ORGANIZATION";
                worksheet.Cell(currentRow, 8).Value = "ORGANIZATION_SIZE";
                worksheet.Cell(currentRow, 9).Value = "ADDRESS";
                worksheet.Cell(currentRow, 10).Value = "CITY";
                worksheet.Cell(currentRow, 11).Value = "PROVINCE";
                worksheet.Cell(currentRow, 12).Value = "POSTAL_CODE";
                worksheet.Cell(currentRow, 13).Value = "EMAIL";
                worksheet.Cell(currentRow, 14).Value = "EMERG_CONTACT_NAME";
                worksheet.Cell(currentRow, 15).Value = "EMERG_CONTACT_PHONE";
                worksheet.Cell(currentRow, 16).Value = "ACCESSIBILITY";
                worksheet.Cell(currentRow, 17).Value = "FIRST_NPI_CONFERENCE";
                worksheet.Cell(currentRow, 18).Value = "EXCLUDE_FROM_ATTENDEE_LIST";



                foreach (var expo in _context.Expos.ToList())
                {
                    currentRow++;
                    worksheet.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(currentRow, 1).Value =  expo.Id;
                    worksheet.Cell(currentRow, 2).Value =  expo.NpiNumber;
                    worksheet.Cell(currentRow, 3).Value =  expo.FirstName;
                    worksheet.Cell(currentRow, 4).Value =  expo.LastName;
                    worksheet.Cell(currentRow, 5).Value =  expo.BadgeName;
                    worksheet.Cell(currentRow, 6).Value =  expo.Title;
                    worksheet.Cell(currentRow, 7).Value =  expo.Organization;
                    worksheet.Cell(currentRow, 8).Value =  expo.NumberOfEmployees;
                    worksheet.Cell(currentRow, 9).Value =  expo.Address;
                    worksheet.Cell(currentRow, 10).Value = expo.City;
                    worksheet.Cell(currentRow, 11).Value = expo.Province;
                    worksheet.Cell(currentRow, 12).Value = expo.PostalCode;
                    worksheet.Cell(currentRow, 13).Value = expo.Email;
                    worksheet.Cell(currentRow, 14).Value = expo.EmergencyContactName;
                    worksheet.Cell(currentRow, 15).Value = expo.EmergencyContactPhoneNumber;
                    worksheet.Cell(currentRow, 16).Value = expo.AccessibilityRequirements;
                    worksheet.Cell(currentRow, 17).Value = expo.FirstNPIConference;
                    worksheet.Cell(currentRow, 18).Value = expo.Distribute;
                }

                using (var stream = new MemoryStream())
                {

                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "ExpoRegistrations.xlsx");
                }
            }
        }

        public IActionResult OnPostSpeakerExcel()
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Speakers");
                var headers = worksheet.Range("A1:R1");
                headers.Style.Font.Bold = true;
                headers.Style.Fill.BackgroundColor = XLColor.Aqua;
                worksheet.Rows(1, 2).Style.Alignment.WrapText = true;

                worksheet.Rows(1, 2).AdjustToContents();
                worksheet.Columns(1, 18).AdjustToContents();
                var currentRow = 1;

                worksheet.Cell(currentRow, 1).Value = "ROWID";
                worksheet.Cell(currentRow, 2).Value = "NPI_NUMBER";
                worksheet.Cell(currentRow, 3).Value = "FIRST_NAME";
                worksheet.Cell(currentRow, 4).Value = "LAST_NAME";
                worksheet.Cell(currentRow, 5).Value = "BADGE";
                worksheet.Cell(currentRow, 6).Value = "SPEAKING_DATE";
                worksheet.Cell(currentRow, 7).Value = "TITLE";
                worksheet.Cell(currentRow, 8).Value = "ORGANIZATION";
                worksheet.Cell(currentRow, 9).Value = "ORGANIZATION_SIZE";
                worksheet.Cell(currentRow, 10).Value = "ADDRESS";
                worksheet.Cell(currentRow, 11).Value = "CITY";
                worksheet.Cell(currentRow, 12).Value = "PROVINCE";
                worksheet.Cell(currentRow, 13).Value = "POSTAL_CODE";
                worksheet.Cell(currentRow, 14).Value = "EMAIL";
                worksheet.Cell(currentRow, 15).Value = "EMERG_CONTACT_NAME";
                worksheet.Cell(currentRow, 16).Value = "EMERG_CONTACT_PHONE";
                worksheet.Cell(currentRow, 17).Value = "ACCESSIBILITY";
                worksheet.Cell(currentRow, 18).Value = "FIRST_NPI_CONFERENCE";
                worksheet.Cell(currentRow, 19).Value = "EXCLUDE_FROM_ATTENDEE_LIST";



                foreach (var speaker in _context.Speakers.ToList())
                {
                    currentRow++;
                    worksheet.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(currentRow, 1).Value = speaker.Id;
                    worksheet.Cell(currentRow, 2).Value = speaker.NpiNumber;
                    worksheet.Cell(currentRow, 3).Value = speaker.FirstName;
                    worksheet.Cell(currentRow, 4).Value = speaker.LastName;
                    worksheet.Cell(currentRow, 5).Value = speaker.BadgeName;
                    worksheet.Cell(currentRow, 6).Value = speaker.SpeakingDate;
                    worksheet.Cell(currentRow, 6).Value = speaker.Title;
                    worksheet.Cell(currentRow, 7).Value = speaker.Organization;
                    worksheet.Cell(currentRow, 8).Value = speaker.NumberOfEmployees;
                    worksheet.Cell(currentRow, 9).Value = speaker.Address;
                    worksheet.Cell(currentRow, 10).Value = speaker.City;
                    worksheet.Cell(currentRow, 11).Value = speaker.Province;
                    worksheet.Cell(currentRow, 12).Value = speaker.PostalCode;
                    worksheet.Cell(currentRow, 13).Value = speaker.Email;
                    worksheet.Cell(currentRow, 14).Value = speaker.EmergencyContactName;
                    worksheet.Cell(currentRow, 15).Value = speaker.EmergencyContactPhoneNumber;
                    worksheet.Cell(currentRow, 16).Value = speaker.AccessibilityRequirements;
                    worksheet.Cell(currentRow, 17).Value = speaker.FirstNPIConference;
                    worksheet.Cell(currentRow, 18).Value = speaker.Distribute;
                }

                using (var stream = new MemoryStream())
                {

                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "SpeakerRegistrations.xlsx");
                }
            }
        }
    }
}
