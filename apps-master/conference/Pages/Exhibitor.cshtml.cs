﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using apps.Context;
using apps.Models;

namespace apps.Pages
{
    public class ExhibitorModel : PageModel
    {
        private readonly ConferenceContext _context;

        public ExhibitorModel(ConferenceContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Exhibitor Exhibitor { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Exhibitors.Add(Exhibitor);
            await _context.SaveChangesAsync();

            return RedirectToPage("./ThankYou");
        }
    }
}
