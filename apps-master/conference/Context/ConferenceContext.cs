﻿using apps.Models;
using Microsoft.EntityFrameworkCore;

namespace apps.Context
{
    public class ConferenceContext:DbContext
    {

        public ConferenceContext(DbContextOptions<ConferenceContext> options) : base(options)
        {
        }
      
        public DbSet<Exhibitor> Exhibitors{ get; set; }
        public DbSet<Expo> Expos{ get; set; }
        public DbSet<Speaker> Speakers { get; set; }
        public DbSet<Bootcamp> Bootcamps { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<FunNight> FunNights { get; set; }
    }
}
