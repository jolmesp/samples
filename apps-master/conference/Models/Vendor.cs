﻿using System.ComponentModel.DataAnnotations;

namespace apps.Models
{
    public class Vendor
    {
        public int Id { get; set; }

        [Display(Name = "Are you a member of the NPI?")]
        public bool? IsMember { get; set; }

        [Display(Name = "NPI Number")]
        public int? NpiNumber { get; set; }

        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; }

        [Display(Name = "Name for the Badge")]
        [Required]
        public string BadgeName { get; set; }

        [Display(Name = "Title")]
        public string? Title { get; set; }

        [Required]
        public string Organization { get; set; }

        [Display(Name = "Organization Size")]
        public string? NumberOfEmployees { get; set; }


        public string? Address { get; set; }


        public string? City { get; set; }


        public string? Province { get; set; }

        [Display(Name = "Postal Code")]
        public string? PostalCode { get; set; }

        [Display(Name = "Phone")]
        public string? Phone { get; set; }

        [Display(Name = "Email")]
        [EmailAddress]
        public string? Email { get; set; }


        [Required]
        [Display(Name = "Emergency Contact Name")]
        public string? EmergencyContactName { get; set; }

        [Required]
        [Display(Name = "Emergency Contact Phone")]
        public string? EmergencyContactPhoneNumber { get; set; }

        [Display(Name = "Accessibility Requirements (e.g. wheelchair, etc.)")]
        public string? AccessibilityRequirements { get; set; }

        [Display(Name = "Is this your first NPI Conference?")]
        public bool? FirstNPIConference { get; set; }

        [Display(Name = "I agree to receive exciting news, information and other marketing communications directly from our sponsors and exhibitors of The Institute Annual Conference and Trade Show")]
        [Required(ErrorMessage = "This field is required")]

        public bool Distribute { get; set; }

        [Required]
        public bool ReadWaiver { get; set; }

        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
