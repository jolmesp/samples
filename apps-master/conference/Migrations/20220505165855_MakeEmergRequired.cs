﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace apps.Migrations
{
    public partial class MakeEmergRequired : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Vendors",
                keyColumn: "EmergencyContactPhoneNumber",
                keyValue: null,
                column: "EmergencyContactPhoneNumber",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactPhoneNumber",
                table: "Vendors",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Vendors",
                keyColumn: "EmergencyContactName",
                keyValue: null,
                column: "EmergencyContactName",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactName",
                table: "Vendors",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Speakers",
                keyColumn: "EmergencyContactPhoneNumber",
                keyValue: null,
                column: "EmergencyContactPhoneNumber",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactPhoneNumber",
                table: "Speakers",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Speakers",
                keyColumn: "EmergencyContactName",
                keyValue: null,
                column: "EmergencyContactName",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactName",
                table: "Speakers",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Expos",
                keyColumn: "EmergencyContactPhoneNumber",
                keyValue: null,
                column: "EmergencyContactPhoneNumber",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactPhoneNumber",
                table: "Expos",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Expos",
                keyColumn: "EmergencyContactName",
                keyValue: null,
                column: "EmergencyContactName",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactName",
                table: "Expos",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Exhibitors",
                keyColumn: "EmergencyContactPhoneNumber",
                keyValue: null,
                column: "EmergencyContactPhoneNumber",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactPhoneNumber",
                table: "Exhibitors",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Exhibitors",
                keyColumn: "EmergencyContactName",
                keyValue: null,
                column: "EmergencyContactName",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactName",
                table: "Exhibitors",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Bootcamps",
                keyColumn: "EmergencyContactPhoneNumber",
                keyValue: null,
                column: "EmergencyContactPhoneNumber",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactPhoneNumber",
                table: "Bootcamps",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Bootcamps",
                keyColumn: "EmergencyContactName",
                keyValue: null,
                column: "EmergencyContactName",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactName",
                table: "Bootcamps",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactPhoneNumber",
                table: "Vendors",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactName",
                table: "Vendors",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactPhoneNumber",
                table: "Speakers",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactName",
                table: "Speakers",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactPhoneNumber",
                table: "Expos",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactName",
                table: "Expos",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactPhoneNumber",
                table: "Exhibitors",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactName",
                table: "Exhibitors",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactPhoneNumber",
                table: "Bootcamps",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "EmergencyContactName",
                table: "Bootcamps",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");
        }
    }
}
