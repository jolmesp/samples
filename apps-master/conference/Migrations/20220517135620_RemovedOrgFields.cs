﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace apps.Migrations
{
    public partial class RemovedOrgFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumberOfEmployees",
                table: "FunNights");

            migrationBuilder.DropColumn(
                name: "Organization",
                table: "FunNights");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NumberOfEmployees",
                table: "FunNights",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Organization",
                table: "FunNights",
                type: "longtext",
                nullable: false)
                .Annotation("MySql:CharSet", "utf8mb4");
        }
    }
}
