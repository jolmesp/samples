var rows = [];
var tmpStage = '';
jQuery('tr').each(function (index) {
    var row = {};
    var count = index + 1; 
 
    if(jQuery(this).children('td:nth-child(1)').length > 0){

        var stage = jQuery(this).children('td:nth-child(1)')[0].innerHTML;
        var item = jQuery(this).children('td:nth-child(2)')[0].innerHTML;
        if(stage.length != 0 ){
            tmpStage = stage;
            console.log(tmpStage);
        }

        row["order"] = count;
        row["stage"] = tmpStage;
        row["item"] = item;
        row["selected"] = false;
        rows.push(row); 
    }

});
 
console.log(rows)