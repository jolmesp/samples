export const french = {
    "hiringProcedure": [
        //stage: action
        {
            order: 1,
            stage: "Détermination du statut d'emploi",
            item: "Analyser les conditions de travail afin de déterminer si la personne sera un employé (salarié) ou un travailleur indépendant (autonome).",
            selected: false
        },

        {
            order: 2,
            stage: "Autorisation de recruter",
            item: "Obtenir l'autorisation de procéder à l'appel de candidatures.",
            selected: false
        },

        {
            order: 3,
            stage: "Description de poste",
            item: "Rédiger la description du poste.",
            selected: false
        },

        {
            order: 4,
            stage: "Appel de candidatures",
            item: "Rédiger l'appel de candidatures.",
            selected: false
        },
        {
            order: 5,
            stage: "Offres d'emploi / curriculum vitae",
            item: "Faire l'étude des offres d'emploi / curriculum vitae.",
            selected: false
        }, 
        {
            order: 6,
            stage: "Entrevues",
            item: "Présélectionner les candidats.",
            selected: false
        }, 
        {
            order: 7,
            stage: "Entrevues",
            item: "Recevoir les candidats en entrevue.",
            selected: false
        }, 
        {
            order: 8,
            stage: "Références",
            item: "Obtenir l'autorisation écrite de vérifier les références, y compris, le cas échéant, le dossier de crédit, le dossier criminel, le dossier de sécurité, l'admissibilité à cautionnement, le dossier d'immigration et le dossier de conduite automobile (le service de la paie ne devrait PAS avoir accès à ces détails, même s'ils sont exigés par le service des ressources humaines, le comité d'embauche ou la convention collective).",
            selected: false
        }, 
        {
            order: 9,
            stage: "Lettre d'offre / contrat d'emploi",
            item: "Rédiger une lettre d'offre / un contrat d'emploi qui définit les conditions suivantes :",
            selected: false
        }, 
        {
            order: 10,
            stage: "Lettre d'offre / contrat d'emploi",
            item: "la période d'essai;",
            selected: false
        }, 

        {
            order: 11,
            stage: "Lettre d'offre / contrat d'emploi",
            item: "le statut d'emploi (employé, professionnel, cadre, cadre supérieur, etc.);",
            selected: false
        }, 
        {
            order: 12,
            stage: "Lettre d'offre / contrat d'emploi",
            item: "la structure salariale (taux de salaire de base, taux supplémentaire, commissions, primes, actions, vacances, jours fériés, congés pour motifs personnels, etc.);",
            selected: false
        }, 
        {
            order: 13,
            stage: "Lettre d'offre / contrat d'emploi",
            item: "les avantages sociaux (la documentation du régime d'avantages sociaux doit être envoyée à l'employé avant le premier jour d'emploi).",
            selected: false
        }, 
      
        {
            order: 14,
            stage: "Embauche",
            item: "Embaucher la personne dont la candidature est retenue.",
            selected: false
        }, 
        {
            order: 15,
            stage: "Autres",
            item: "",
            selected: false
        }, 
    ],

    "priorToStartDate":[
        {
            order: 1,
            stage: "Lettre d'acceptation",
            item: "La personne dont la candidature est retenue a accepté l'offre d'emploi.",
            selected: false
        },

        {
            order: 2,
            stage: "Lettre d'acceptation",
            item: "Date, heure et endroit où se présenter.",
            selected: false
        },

        {
            order: 3,
            stage: "Lettre d'acceptation",
            item: "À qui se présenter.",
            selected: false
        },
        {
            order: 4,
            stage: "Lettre d'acceptation",
            item: "Renseignements sur le stationnement",
            selected: false
        },

        {
            order: 5,
            stage: "Lettre d'acceptation",
            item: "Fréquence de paie, retenues à la source et date de la première paie.",
            selected: false
        },
   
        {
            order: 7,
            stage: "Annonce au personnel",
            item: "Rédiger une annonce au personnel pour présenter le nouvel employé.",
            selected: false
        },
        {
            order: 8,
            stage: "Programme d'orientation / d'initiation",
            item: "Organiser le programme d'orientation / d'initiation et le transmettre à tous les services et employés nécessaires.",
            selected: false
        },
        {
            order: 9,
            stage: "Organisation physique",
            item: "Bureau et plaques nominatives.",
            selected: false
        },
        {
            order: 10,
            stage: "Organisation physique",
            item: "Ameublement nécessaire.",
            selected: false
        },
        {
            order: 11,
            stage: "Organisation physique",
            item: "Assignation d'un casier pour le courrier.",
            selected: false
        },
        {
            order: 12,
            stage: "Organisation physique",
            item: "Ajout aux répertoires téléphonique et d'adresses courriel de l'organisation.",
            selected: false
        },
        {
            order: 13,
            stage: "Organisation physique",
            item: "Autorisations d'accès de l'employé.",
            selected: false
        },
        {
            order: 14,
            stage: "Prendre les mesures d'adaptation nécessaires pour accommoder un handicap, le cas échéant.",
            item: "Prendre les mesures d'adaptation nécessaires pour accommoder un handicap, le cas échéant.",
            selected: false
        },
        {
            order: 15,
            stage: "Place de stationnement",
            item: "Réserver/assigner une place de stationnement, si nécessaire.",
            selected: false
        },
        {
            order: 16,
            stage: "Accès de sécurité",
            item: "Prendre les mesures nécessaires pour permettre l'accès de l'employé aux immeubles et lui remettre une clé, un insigne, les codes d'alarme, etc.",
            selected: false
        },

        {
            order: 17,
            stage: "Configuration des TI",
            item: "Ordinateur de bureau/portable, imprimante, mot de passe d'accès.",
            selected: false
        },
        {
            order: 18,
            stage: "Configuration des TI",
            item: "Adresse de courriel.",
            selected: false
        },
        {
            order: 19,
            stage: "Configuration des TI",
            item: "Accès au réseau, y compris au réseau privé virtuel (VPN), si nécessaire.",
            selected: false
        },
        {
            order: 20,
            stage: "Configuration des TI",
            item: "Téléphone fixe, code pour les interurbains, activation de la boîte vocale, mot de passe, message d'accueil.",
            selected: false
        },
        {
            order: 21,
            stage: "Configuration des TI",
            item: "Téléphone portable / intelligent / assistant numérique de poche",
            selected: false
        },
        {
            order: 22,
            stage: "Configuration des TI",
            item: "Organisation du bureau à domicile, si nécessaire.",
            selected: false
        },
        {
            order: 23,
            stage: "Autres",
            item: "",
            selected: false
        },

    ],

    "startDateAndBeyond":[
        {
            "order": 2,
            "stage": "Site web / infolettre de l'organisation",
            "item": "Ajouter la présentation biographique de l'employé sur le site web   (Intranet et/ou Internet) et/ou la publier dans l'infolettre.",
            "selected": false
        },
        {
            "order": 3,
            "stage": "Annonces dans les journaux et les publications industrielles",
            "item": "Annoncer l'embauche dans les journaux et/ou dans les publications   industrielles, le cas échéant.",
            "selected": false
        },
        {
            "order": 4,
            "stage": "Communications internes",
            "item": "Demander à l'employé le nom usuel qu'il souhaite utiliser dans   les communications internes.",
            "selected": false
        },
        {
            "order": 5,
            "stage": "À joindre en cas d'urgence",
            "item": "Demander à l'employé de fournir les coordonnées d'une personne à   joindre en cas d'urgence.",
            "selected": false
        },
        {
            "order": 6,
            "stage": "Autorisation d'embauche",
            "item": "Obtenir l'autorisation d'embauche du service des ressources   humaines ou du gestionnaire responsable.",
            "selected": false
        },
        {
            "order": 7,
            "stage": "TD1 / TP-1015.3",
            "item": "Obtenir les exemplaires des formulaires de demande de crédits   d'impôt personnels TD1 / TP-1015.3 remplis par l'employé et comportant son nom   légal et son NAS.",
            "selected": false
        },
        {
            "order": 8,
            "stage": "Statut aux fins du RPC",
            "item": "Demander une copie du formulaire CPT30 si l'employé a de 65 à 70   ans, qu'il reçoit une pension de retraite du RPC ou du RRQ et qu'il ne veut plus   cotiser au RPC.",
            "selected": false
        },
        {
            "order": 9,
            "stage": "Numéro d'assurance sociale (NAS)",
            "item": "Valider le NAS de l'employé (s'il s'agit d'un numéro temporaire de la série 900, vérifier la date d'expiration ainsi que la validité du permis   de travail).",
            "selected": false
        },
        {
            "order": 10,
            "stage": "Numéro d'assurance sociale (NAS)",
            "item": "L'employeur doit pouvoir faire cette vérification dans les trois   jours de la date d'embauche (des pénalités peuvent s'ensuivre si l'employé   refuse).",
            "selected": false
        },
        {
            "order": 11,
            "stage": "Numéro d'assurance sociale (NAS)",
            "item": "Pour assurer la protection des renseignements personnels, ne pas   conserver de photocopie.",
            "selected": false
        },
        {
            "order": 12,
            "stage": "Adresse de l'employé",
            "item": "Aux fins des feuillets T4, des bulletins de paie, des assurances   collectives.",
            "selected": false
        },
        {
            "order": 13,
            "stage": "Date de naissance",
            "item": "Aux fins du RPC, des assurances collectives, du régime de   retraite de l'organisation, du REER.",
            "selected": false
        },
        {
            "order": 14,
            "stage": "Codification du système de paie",
            "item": "Codification des données concernant l'employé dans le système de   paie, imputation au service et aux centres de coûts appropriés.",
            "selected": false
        },
        {
            "order": 15,
            "stage": "Codification du système de paie",
            "item": "Codification en fonction des heures de présence ou comme salarié   et ajout des exceptions.",
            "selected": false
        },
        {
            "order": 16,
            "stage": "Codification du système de paie",
            "item": "Vérification que l'employé est codifié au bon territoire de   compétence aux fins des normes d'emploi / du travail.",
            "selected": false
        },
        {
            "order": 17,
            "stage": "Chèque(s) annulé(s)",
            "item": "Dépôt direct, répartition de la paie nette (entièrement déposée   dans un compte ou répartie dans plusieurs comptes).",
            "selected": false
        },
        {
            "order": 18,
            "stage": "Autorisation pour le bulletin de paie électronique",
            "item": "Obtenir l'autorisation écrite de l'employé pour produire ses   bulletins de paie par un moyen électronique, s'il ne s'agit pas déjà d'une   condition d'emploi.",
            "selected": false
        },
        {
            "order": 19,
            "stage": "Autorisation pour les feuillets T4/Relevé 1 électroniques",
            "item": "Obtenir l'autorisation écrite de l'employé pour la remise des   déclarations de fin d'année par un moyen électronique.",
            "selected": false
        },
        {
            "order": 20,
            "stage": "Formulaires d'inscription / de renonciation",
            "item": "Obtenir les formulaires d'inscription / de renonciation pour les   assurances collectives, y compris les renseignements concernant le bénéficiaire   (ou le fiduciaire si le bénéficiaire est mineur).",
            "selected": false
        },
        {
            "order": 21,
            "stage": "Pension",
            "item": "Obtenir les formulaires d'inscription / de renonciation pour le   régime de retraite / RPDB / REER / CELI (si l'employé a déjà cotisé à un régime   de retraite au cours de l'année fiscale, vérifier les maximums de l'ARC).",
            "selected": false
        },
        {
            "order": 22,
            "stage": "Convention collective (le cas échéant)",
            "item": "Adhésion signée au syndicat.",
            "selected": false
        },
        {
            "order": 23,
            "stage": "Convention collective (le cas échéant)",
            "item": "Cotisations et droit d'adhésion.",
            "selected": false
        },
        {
            "order": 24,
            "stage": "Automobile / véhicule",
            "item": "Allocation pour automobile / remboursement des dépenses si   l'employé utilise son propre véhicule.",
            "selected": false
        },
        {
            "order": 25,
            "stage": "Automobile / véhicule",
            "item": "Automobile / véhicule remis à (choisi par) l'employé si   fourni.",
            "selected": false
        },
        {
            "order": 26,
            "stage": "Automobile / véhicule",
            "item": "Plaque d'immatriculation ________________________",
            "selected": false
        },
        {
            "order": 27,
            "stage": "Automobile / véhicule",
            "item": "No de la police d'assurance______________________",
            "selected": false
        },
        {
            "order": 28,
            "stage": "Automobile / véhicule",
            "item": "Lecture de l'odomètre _____________________",
            "selected": false
        },
        {
            "order": 29,
            "stage": "Automobile / véhicule",
            "item": "Fournir un journal de bord et un guide détaillé et illustré sur   de la façon de le remplir.",
            "selected": false
        },
        {
            "order": 30,
            "stage": "Automobile / véhicule",
            "item": "Assigner une place de stationnement, le cas échéant.",
            "selected": false
        },
        {
            "order": 31,
            "stage": "Carte de crédit d'affaires",
            "item": "Expliquer et faire signer la politique de l'organisation   relativement aux dépenses admises.",
            "selected": false
        },
        {
            "order": 32,
            "stage": "Carte de crédit d'affaires",
            "item": "Faire émettre une carte de crédit d'affaires au nom de   l'employé.",
            "selected": false
        },
        {
            "order": 33,
            "stage": "Carte d'escompte d'employé",
            "item": "Expliquer et faire signer la politique de l'organisation   relativement aux escomptes.",
            "selected": false
        },
        {
            "order": 34,
            "stage": "Carte d'escompte d'employé",
            "item": "Émettre une carte d'escompte au nom de l'employé.",
            "selected": false
        },
        {
            "order": 35,
            "stage": "Logement et repas",
            "item": "Le cas échéant, prendre les dispositions nécessaires pour le   logement et les repas, ou verser une allocation de subsistance.",
            "selected": false
        },
        {
            "order": 36,
            "stage": "Titres de transport / coupons de taxi",
            "item": "Faire émettre des titres de transport pour l'avion, le train ou   le taxi, le cas échéant.",
            "selected": false
        },
        {
            "order": 37,
            "stage": "Services de bureau",
            "item": "Évaluation ergonomique.",
            "selected": false
        },
        {
            "order": 38,
            "stage": "Services de bureau",
            "item": "Meubles additionnels nécessaires.",
            "selected": false
        },
        {
            "order": 39,
            "stage": "Services de bureau",
            "item": "Commande de cartes d'affaires.",
            "selected": false
        },
        {
            "order": 40,
            "stage": "Services de bureau",
            "item": "Liste téléphonique pour la réception et les principaux   postes.",
            "selected": false
        },
        {
            "order": 41,
            "stage": "Services de bureau",
            "item": "Assignation d'un casier pour le courrier.",
            "selected": false
        },
        {
            "order": 42,
            "stage": "Services de bureau",
            "item": "Fonctionnement du courrier interservices.",
            "selected": false
        },
        {
            "order": 43,
            "stage": "Services de bureau",
            "item": "Instructions relatives aux services de messagerie.",
            "selected": false
        },
        {
            "order": 44,
            "stage": "Services de bureau",
            "item": "Procédure de réservation de la salle de conférence.",
            "selected": false
        },
        {
            "order": 45,
            "stage": "Services de bureau",
            "item": "Ressources Intranet / bibliothèque.",
            "selected": false
        },
        {
            "order": 46,
            "stage": "Services de bureau",
            "item": "Procédure pour la commande de fournitures.",
            "selected": false
        },
        {
            "order": 47,
            "stage": "Fournitures de bureau",
            "item": "Remettre des fournitures de bureau, que l'employé soit installé   dans l'établissement ou à son domicile.",
            "selected": false
        },
        {
            "order": 48,
            "stage": "Renseignements sur l'organisation",
            "item": "Renseignements de nature générale sur l'organisation, notamment   :<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• historique;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• organigrammes;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• principaux   services/responsables;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• comités bénévoles;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• activités   sociales;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• engagement communautaire;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• services de proximité /   voisinage;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• gym, cafétéria, restaurants;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• plans des étages et   des bureaux.",
            "selected": false
        },
        {
            "order": 49,
            "stage": "Sécurité et évacuation",
            "item": "Trousses de premiers soins.",
            "selected": false
        },
        {
            "order": 50,
            "stage": "Sécurité et évacuation",
            "item": "Défibrillateurs.",
            "selected": false
        },
        {
            "order": 51,
            "stage": "Sécurité et évacuation",
            "item": "Extincteurs.",
            "selected": false
        },
        {
            "order": 52,
            "stage": "Sécurité et évacuation",
            "item": "Pompiers et services d'urgence.",
            "selected": false
        },
        {
            "order": 53,
            "stage": "Politiques, instructions, lignes directrices et accusés de réception",
            "item": "Sécurité au travail.",
            "selected": false
        },
        {
            "order": 54,
            "stage": "Politiques, instructions, lignes directrices et accusés de réception",
            "item": "Usages admis des ressources de l'organisation.",
            "selected": false
        },
        {
            "order": 55,
            "stage": "Politiques, instructions, lignes directrices et accusés de réception",
            "item": "Disposition des déchets et respect de l'environnement (p. ex.,   recyclage, cueillette des déchets organiques et de papier, impression   recto-verso, etc.).",
            "selected": false
        },
        {
            "order": 56,
            "stage": "Politiques, instructions, lignes directrices et accusés de réception",
            "item": "Responsabilité sociale collective.",
            "selected": false
        },
        {
            "order": 57,
            "stage": "Politiques, instructions, lignes directrices et accusés de réception",
            "item": "Bonne conduite du personnel.",
            "selected": false
        },
        {
            "order": 58,
            "stage": "Politiques, instructions, lignes directrices et accusés de réception",
            "item": "Diversité et inclusion.",
            "selected": false
        },
        {
            "order": 59,
            "stage": "Politiques, instructions, lignes directrices et accusés de réception",
            "item": "Harcèlement.",
            "selected": false
        },
        {
            "order": 60,
            "stage": "Politiques, instructions, lignes directrices et accusés de réception",
            "item": "Formation et ressources.",
            "selected": false
        },
        {
            "order": 61,
            "stage": "Politiques, instructions, lignes directrices et accusés de réception",
            "item": "Évaluations de rendement (buts, objectifs) et gestion des talents   (remboursement des frais de scolarité).",
            "selected": false
        },
        {
            "order": 62,
            "stage": "Politiques, instructions, lignes directrices et accusés de réception",
            "item": "Absences (haute saison, fermeture des fêtes, périodes   d'interdiction).",
            "selected": false
        },
        {
            "order": 63,
            "stage": "Politiques, instructions, lignes directrices et accusés de réception",
            "item": "Examens médicaux des dirigeants exécutifs.",
            "selected": false
        },
        {
            "order": 64,
            "stage": "Guide de l'employé",
            "item": "Remettre un guide de l'employé et offrir des explications.",
            "selected": false
        },
        {
            "order": 65,
            "stage": "Guide de l'employé",
            "item": "Obtenir un accusé de réception du guide de l'employé et des   politiques.",
            "selected": false
        },
        {
            "order": 66,
            "stage": "Autres",
            "item": "",
            "selected": false
        }
    ],
    "employeeHandbookContents":[
        {
            "order": 2,
            "stage": "Période d'essai",
            "item": "Durée, évaluation des progrès.",
            "selected": false
        },
        {
            "order": 3,
            "stage": "Heures de travail",
            "item": "Heures régulières, heures supplémentaires, banque d'heures   compensatoires, indemnité de présence / de disponibilité, horaire d'été.",
            "selected": false
        },
        {
            "order": 4,
            "stage": "Périodes de repos et pauses",
            "item": "Admissibilité.",
            "selected": false
        },
        {
            "order": 5,
            "stage": "Vacances",
            "item": "Admissibilité, procédure de la demande, mise de côté / report des   vacances.",
            "selected": false
        },
        {
            "order": 6,
            "stage": "Jours fériés",
            "item": "Admissibilité, rémunération des jours fériés.",
            "selected": false
        },
        {
            "order": 7,
            "stage": "Célébrations religieuses",
            "item": "Admissibilité, procédure de demande.",
            "selected": false
        },
        {
            "order": 8,
            "stage": "Congés flexibles",
            "item": "Admissibilité, procédure de demande.",
            "selected": false
        },
        {
            "order": 9,
            "stage": "Congés de maladie",
            "item": "Admissibilité, billet de médecin, congés non utilisés.",
            "selected": false
        },
        {
            "order": 10,
            "stage": "Congés pour motifs personnels",
            "item": "Admissibilité, procédure de demande.",
            "selected": false
        },
        {
            "order": 11,
            "stage": "Congé de décès",
            "item": "Admissibilité, définition des types de relations couverts.",
            "selected": false
        },
        {
            "order": 12,
            "stage": "Perfectionnement professionnel",
            "item": "Admissibilité, autorisation préalable.",
            "selected": false
        },
        {
            "order": 13,
            "stage": "Fonction de juré",
            "item": "Expliquer si le salaire sera maintenu et si l'employé devra   rembourser les allocations éventuellement consenties par le tribunal.",
            "selected": false
        },
        {
            "order": 14,
            "stage": "Congé de maternité / de paternité / parental / d'adoption",
            "item": "Admissibilité, préavis à l'employeur, dispositions visant un   complément aux prestations.",
            "selected": false
        },
        {
            "order": 15,
            "stage": "Invalidité de courte / longue durée",
            "item": "Conditions d'admissibilité, procédure / formulaire de   demande.",
            "selected": false
        },
        {
            "order": 16,
            "stage": "Congé de compassion",
            "item": "Admissibilité, préavis à l'employeur.",
            "selected": false
        },
        {
            "order": 17,
            "stage": "Congé de réserviste",
            "item": "Admissibilité, préavis à l'employeur.",
            "selected": false
        },
        {
            "order": 18,
            "stage": "Régime collectif d'avantages sociaux",
            "item": "Admissibilité, partie de la prime à payer par l'employeur (le cas   échéant).",
            "selected": false
        },
        {
            "order": 19,
            "stage": "Régime de retraite / RPDB / REER / CELI",
            "item": "Admissibilité, cotisation de l'employé (le cas échéant).",
            "selected": false
        },
        {
            "order": 20,
            "stage": "Avantages / allocations imposables",
            "item": "Admissibilité, fréquence de déclaration / paiement.",
            "selected": false
        },
        {
            "order": 21,
            "stage": "Primes",
            "item": "Rendement, congés fériés, discrétionnaires.",
            "selected": false
        },
        {
            "order": 22,
            "stage": "Prix pour états de service",
            "item": "Admissibilité.",
            "selected": false
        },
        {
            "order": 23,
            "stage": "Frais de scolarité",
            "item": "Admissibilité, autorisation préalable, demande de   remboursement.",
            "selected": false
        },
        {
            "order": 24,
            "stage": "Retenues sur la paie",
            "item": "Explication des retenues : obligatoires, obligatoires de   l'organisation, facultatives.",
            "selected": false
        },
        {
            "order": 25,
            "stage": "Remboursement de dépenses",
            "item": "Autorisation préalable, demande de remboursement.",
            "selected": false
        },
        {
            "order": 26,
            "stage": "Saisie-arrêt sur salaire",
            "item": "Explication des responsabilités de l'employé et de   l'employeur.",
            "selected": false
        },
        {
            "order": 27,
            "stage": "Sécurité / indemnisation des travailleurs",
            "item": "Politique en matière de sécurité, formulaires, paiements.",
            "selected": false
        },
        {
            "order": 28,
            "stage": "Internet / matériel informatique de l'organisation",
            "item": "Explication des politiques.",
            "selected": false
        },
        {
            "order": 29,
            "stage": "Téléphone fixe / portable",
            "item": "Explication des politiques.",
            "selected": false
        },
        {
            "order": 30,
            "stage": "Déplacements",
            "item": "Politique en matière de temps de déplacement (au moins conforme   aux normes d'emploi / du travail).",
            "selected": false
        },
        {
            "order": 31,
            "stage": "Déplacements",
            "item": "Procédure d'autorisation.",
            "selected": false
        },
        {
            "order": 32,
            "stage": "Déplacements",
            "item": "Agent(s) de voyage / réservation en ligne.",
            "selected": false
        },
        {
            "order": 33,
            "stage": "Déplacements",
            "item": "Services de taxi / limousine.",
            "selected": false
        },
        {
            "order": 34,
            "stage": "Déplacements",
            "item": "Liste des hôtels préférés / codes d'escompte.",
            "selected": false
        },
        {
            "order": 35,
            "stage": "Déplacements",
            "item": "Options offertes relativement aux soins aux enfants / animaux /   aînés / à domicile durant les absences.",
            "selected": false
        },
        {
            "order": 36,
            "stage": "Responsabilité touchant la consommation d'alcool aux événements   de l'organisation",
            "item": "Explication des politiques et signature d'un accusé de   réception.",
            "selected": false
        },
        {
            "order": 37,
            "stage": "Harcèlement / intimidation",
            "item": "Explication de la politique.",
            "selected": false
        },
        {
            "order": 38,
            "stage": "Code vestimentaire",
            "item": "Explication de la politique.",
            "selected": false
        },
        {
            "order": 39,
            "stage": "Protection des renseignements personnels / confidentialité",
            "item": "Explication des politiques concernant les collègues, les clients,   les bénévoles, le public.",
            "selected": false
        },
        {
            "order": 40,
            "stage": "Fraude",
            "item": "Explication des actes présumés frauduleux et de leurs   répercussions.",
            "selected": false
        },
        {
            "order": 41,
            "stage": "Cessation d'emploi",
            "item": "Motifs de la cessation d'emploi.",
            "selected": false
        },
        {
            "order": 42,
            "stage": "Cessation d'emploi",
            "item": "Explication des paiements admissibles à la cessation d'emploi   (selon la loi ou selon la politique de l'organisation, si c'est plus   avantageux).",
            "selected": false
        },
        {
            "order": 43,
            "stage": "Cessation d'emploi",
            "item": "Relevé d'emploi (RE).",
            "selected": false
        },
        {
            "order": 44,
            "stage": "Annexes",
            "item": "Modèle de registre de présence avec exemple illustré (le cas   échéant).",
            "selected": false
        },
        {
            "order": 45,
            "stage": "Annexes",
            "item": "Formulaires de demande de congé (vacances, banque d'heures   compensatoires, célébrations religieuses, congés flexibles / pour motifs   personnels, congés pour perfectionnement professionnel, congé de décès, congé   pour fonction de juré).",
            "selected": false
        },
        {
            "order": 46,
            "stage": "Annexes",
            "item": "Transfert REER pour allocation de retraite / prime / autre.",
            "selected": false
        },
        {
            "order": 47,
            "stage": "Annexes",
            "item": "Formulaire de remboursement de frais de scolarité.",
            "selected": false
        },
        {
            "order": 48,
            "stage": "Annexes",
            "item": "Modèle de formulaire de remboursement de dépenses avec exemple   illustré.",
            "selected": false
        },
        {
            "order": 49,
            "stage": "Annexes",
            "item": "Formulaire d'inscription / de renonciation pour les avantages   sociaux et le régime de retraite / RPDB / REER / CELI de l'organisation.",
            "selected": false
        },
        {
            "order": 50,
            "stage": "Annexes",
            "item": "Formulaire de désignation du bénéficiaire avec exemple   illustré.",
            "selected": false
        },
        {
            "order": 51,
            "stage": "Annexes",
            "item": "Formulaires de réclamation aux assurances avec exemples   illustrés.",
            "selected": false
        },
        {
            "order": 52,
            "stage": "Annexes",
            "item": "Accusés de réception des politiques de l'organisation signés par   l'employé.",
            "selected": false
        },
        {
            "order": 53,
            "stage": "Annexes",
            "item": "Hyperliens / chemins d'accès et explications nécessaires pour   remplir les documents ci-dessus en ligne sur le réseau de l'organisation ou sur   l'Intranet / l'Internet.",
            "selected": false
        },
        {
            "order": 54,
            "stage": "Autres",
            "item": "",
            "selected": false
        }
    ],
    "integratedPayrollInternet":[
        {
            "order": 2,
            "stage": "Guide de l’employé",
            "item": "Publier le guide complet avec hyperliens de navigation.",
            "selected": false
        },
        {
            "order": 3,
            "stage": "Formulaires d’embauche / de changements aux renseignements sur l’employé",
            "item": "Changement d’adresse.",
            "selected": false
        },
        {
            "order": 4,
            "stage": "Formulaires d’embauche / de changements aux renseignements sur l’employé",
            "item": "Changement de renseignements bancaires.",
            "selected": false
        },
        {
            "order": 5,
            "stage": "Formulaires d’embauche / de changements aux renseignements sur l’employé",
            "item": "Changement de nom (doit correspondre à la carte d’assurance   sociale sur le plan légal, p. ex., pour le feuillet T4 / relevé 1).",
            "selected": false
        },
        {
            "order": 6,
            "stage": "Dossier de présence",
            "item": "Heures régulières, heures supplémentaires, absences   approuvées.",
            "selected": false
        },
        {
            "order": 7,
            "stage": "Demandes de congé",
            "item": "Vacances, congés pour motifs personnels, congés pour célébrations   religieuses, banque d’heures compensatoires, congés pour perfectionnement   professionnel.",
            "selected": false
        },
        {
            "order": 8,
            "stage": "Demandes libre-service",
            "item": "Vacances.",
            "selected": false
        },
        {
            "order": 9,
            "stage": "Demandes libre-service",
            "item": "Congés de maladie.",
            "selected": false
        },
        {
            "order": 10,
            "stage": "Demandes libre-service",
            "item": "Congés pour motifs personnels.",
            "selected": false
        },
        {
            "order": 11,
            "stage": "Demandes libre-service",
            "item": "Banque d’heures supplémentaires / compensatoires.",
            "selected": false
        },
        {
            "order": 12,
            "stage": "Demandes libre-service",
            "item": "Congés pour perfectionnement professionnels.",
            "selected": false
        },
        {
            "order": 13,
            "stage": "Demandes libre-service",
            "item": "Horaire d’été.",
            "selected": false
        },
        {
            "order": 14,
            "stage": "TD1 / TP-1015.3",
            "item": "TD1 électronique / hyperlien vers TP-1015.3.",
            "selected": false
        },
        {
            "order": 15,
            "stage": "CPT30",
            "item": "Hyperlien vers le formulaire CPT30 pour les choix et révocations   de cotisation au RPC.",
            "selected": false
        },
        {
            "order": 16,
            "stage": "Changements RPA / REER / CELI",
            "item": "Demande d’augmentation ou de diminution de la cotisation RPA /   REER / CELI (avec plafonds intégrés).",
            "selected": false
        },
        {
            "order": 17,
            "stage": "Transferts REER",
            "item": "Demande de transfert de l’allocation de retraite / de la prime /   d’un autre type de revenu au REER de l’employé (voir le modèle de formulaire   Transfert direct de la partie admissible d’une allocation de retraite de   l’ACP).",
            "selected": false
        },
        {
            "order": 18,
            "stage": "Journal de bord pour l’automobile / le véhicule",
            "item": "Entrée mensuelle en ligne des entrées au journal de bord.",
            "selected": false
        },
        {
            "order": 19,
            "stage": "Autres",
            "item": "",
            "selected": false
        }
    ], 
}

export const english = {
    "hiringProcedure":[
        {
            "order": 2,
            "stage": "Determination of worker status",
            "item": "Working conditions examined to determine if new worker will be an employee or self-employed worker",
            "selected": false
        },
        {
            "order": 3,
            "stage": "Recruitment authorization",
            "item": "Authorization received to recruit for a new hire",
            "selected": false
        },
        {
            "order": 4,
            "stage": "Job description",
            "item": "Create job description",
            "selected": false
        },
        {
            "order": 5,
            "stage": "Job posting",
            "item": "Create job posting",
            "selected": false
        },
        {
            "order": 6,
            "stage": "Application forms/resumés",
            "item": "Applications/resumés reviewed",
            "selected": false
        },
        {
            "order": 7,
            "stage": "Interview",
            "item": "Prescreen candidate",
            "selected": false
        },
        {
            "order": 8,
            "stage": "Interview",
            "item": "Interview candidate",
            "selected": false
        },
        {
            "order": 9,
            "stage": "References",
            "item": "Obtain written permission to check references, including applicable credit, criminal, security, bondable, immigration and driver’s license verifications (Payroll should NOT be privy to this information even if required by Human Resources/Hiring Committee/Collective Agreement)",
            "selected": false
        },
        {
            "order": 10,
            "stage": "Offer letter/Employment contract",
            "item": "Create offer letter/employment contract that includes:",
            "selected": false
        },
        {
            "order": 11,
            "stage": "Offer letter/Employment contract",
            "item": "Probationary clause",
            "selected": false
        },
        {
            "order": 12,
            "stage": "Offer letter/Employment contract",
            "item": "Employee status (staff, professional, management, senior management, etc.)",
            "selected": false
        },
        {
            "order": 13,
            "stage": "Offer letter/Employment contract",
            "item": "Pay structure with basic salary rate, overtime rate, commission, bonus, stocks, vacation, statutory holidays, personal days, etc.",
            "selected": false
        },
        {
            "order": 14,
            "stage": "Offer letter/Employment contract",
            "item": "Benefit information (benefit package to be sent to employee prior to start date)",
            "selected": false
        },
        {
            "order": 15,
            "stage": "Hire",
            "item": "Hire successful candidate",
            "selected": false
        },
        {
            "order": 16,
            "stage": "Other",
            "item": "",
            "selected": false
        }
    ],
    "priorToStartDate":[
        {
            "order": 2,
            "stage": "Acceptance letter",
            "item": "Successful candidate has accepted offer of employment",
            "selected": false
        },
        {
            "order": 3,
            "stage": "Acceptance letter",
            "item": "When and where to arrive",
            "selected": false
        },
        {
            "order": 4,
            "stage": "Acceptance letter",
            "item": "Who to report to",
            "selected": false
        },
        {
            "order": 5,
            "stage": "Acceptance letter",
            "item": "Parking information",
            "selected": false
        },
        {
            "order": 6,
            "stage": "Acceptance letter",
            "item": "Pay/deduction frequency and first pay date",
            "selected": false
        },
        {
            "order": 7,
            "stage": "Staff announcement",
            "item": "Create staff announcement introducing new employee",
            "selected": false
        },
        {
            "order": 8,
            "stage": "Orientation/induction plan",
            "item": "Set up and circulate an orientation/induction plan to all necessary departments/individuals",
            "selected": false
        },
        {
            "order": 9,
            "stage": "Office set-up",
            "item": "Office space and name plates",
            "selected": false
        },
        {
            "order": 10,
            "stage": "Office set-up",
            "item": "Furniture required",
            "selected": false
        },
        {
            "order": 11,
            "stage": "Office set-up",
            "item": "Mail slot assignment",
            "selected": false
        },
        {
            "order": 12,
            "stage": "Office set-up",
            "item": "Add to company’s email and phone directories",
            "selected": false
        },
        {
            "order": 13,
            "stage": "Office set-up",
            "item": "Access to employee",
            "selected": false
        },
        {
            "order": 14,
            "stage": "Disability accommodations",
            "item": "Set up disability accommodations/modifications if necessary",
            "selected": false
        },
        {
            "order": 15,
            "stage": "Parking space",
            "item": "Arrange/assign parking space if necessary",
            "selected": false
        },
        {
            "order": 16,
            "stage": "Security access",
            "item": "Set up new employee’s access to building(s) and issue key, badge, alarm codes, etc.",
            "selected": false
        },
        {
            "order": 17,
            "stage": "IT setup",
            "item": "Computer/laptop, printer, including password login",
            "selected": false
        },
        {
            "order": 18,
            "stage": "IT setup",
            "item": "Email address",
            "selected": false
        },
        {
            "order": 19,
            "stage": "IT setup",
            "item": "Network access including virtual private network (VPN) if necessary",
            "selected": false
        },
        {
            "order": 20,
            "stage": "IT setup",
            "item": "Office phone/long distance code/voicemail activation, code, script",
            "selected": false
        },
        {
            "order": 21,
            "stage": "IT setup",
            "item": "Cell phone/smart phone/personal digital assistant (PDA)",
            "selected": false
        },
        {
            "order": 22,
            "stage": "IT setup",
            "item": "Home office set-up if necessary",
            "selected": false
        },
        {
            "order": 23,
            "stage": "Other",
            "item": "",
            "selected": false
        }
    ],
    "startDateAndBeyond":[
        {
            "order": 2,
            "stage": "Company website/newsletter",
            "item": "Add new employee’s bio on company website (Intranet and/or Internet) and/or newsletter",
            "selected": false
        },
        {
            "order": 3,
            "stage": "Newspaper and industry announcements",
            "item": "Announce new hire in newspapers and/or industry newsletters if applicable",
            "selected": false
        },
        {
            "order": 4,
            "stage": "Internal communications",
            "item": "Candidate communicates common name for internal communication purposes",
            "selected": false
        },
        {
            "order": 5,
            "stage": "Emergency Contact Details",
            "item": "New employee to provide Emergency Contact Information",
            "selected": false
        },
        {
            "order": 6,
            "stage": "New Hire authorization form",
            "item": "Receive new hire authorization form from Human Resources or manager",
            "selected": false
        },
        {
            "order": 7,
            "stage": "TD1/TP1015.3-V",
            "item": "Receive employee completed TD1/TP-1015.3-V forms with legal name and SIN for personal tax credits purposes",
            "selected": false
        },
        {
            "order": 8,
            "stage": "CPP status",
            "item": "Ask for copy of CPT30 election form if employee is 65-70, in receipt of a CPP/QPP retirement pension and is electing/has elected to stop CPP contributions",
            "selected": false
        },
        {
            "order": 9,
            "stage": "Social Insurance Number (SIN)",
            "item": "Validate employee’s SIN (If temporary 900-series, also need to check expiration date and valid working papers)",
            "selected": false
        },
        {
            "order": 10,
            "stage": "Social Insurance Number (SIN)",
            "item": "Must be available to employer within 3 days of new hire date (subject to penalties if employee refuses)",
            "selected": false
        },
        {
            "order": 11,
            "stage": "Social Insurance Number (SIN)",
            "item": "Do not keep photocopy due to privacy",
            "selected": false
        },
        {
            "order": 12,
            "stage": "Employee address",
            "item": "For T4 issuance, pay statement, group benefits",
            "selected": false
        },
        {
            "order": 13,
            "stage": "Birthdate",
            "item": "For CPP, group benefits, company pension, RRSP",
            "selected": false
        },
        {
            "order": 14,
            "stage": "Payroll set-up",
            "item": "Employee set up on payroll under the correct department/cost centre",
            "selected": false
        },
        {
            "order": 15,
            "stage": "Payroll set-up",
            "item": "Employee set up for either time and attendance or as salaried employee with exception reporting",
            "selected": false
        },
        {
            "order": 16,
            "stage": "Payroll set-up",
            "item": "Employee set up in correct location for employment/labour standards",
            "selected": false
        },
        {
            "order": 17,
            "stage": "Voided cheque(s)",
            "item": "For direct deposit and allocation of net pay (100% in one account, or split if multiple accounts are permitted)",
            "selected": false
        },
        {
            "order": 18,
            "stage": "Electronic pay statement authorization",
            "item": "Written employee authorization received for electronic pay statements if not a valid condition of employment",
            "selected": false
        },
        {
            "order": 19,
            "stage": "Electronic T4/RL-1 authorization",
            "item": "Written employee authorization received for electronic year-end slips",
            "selected": false
        },
        {
            "order": 20,
            "stage": "Benefits enrolment/waiver forms",
            "item": "Enrolment/waiver forms received for group benefits including beneficiary information (should also name a trustee if beneficiary is a minor)",
            "selected": false
        },
        {
            "order": 21,
            "stage": "Pension",
            "item": "Enrolment/waiver forms for company pension plan/DPSP/RRSP/TFSA (If employee already contributed to another plan this tax year, watch CRA maximums)",
            "selected": false
        },
        {
            "order": 22,
            "stage": "Collective agreement information (if applicable)",
            "item": "Signed union enrolment",
            "selected": false
        },
        {
            "order": 23,
            "stage": "Collective agreement information (if applicable)",
            "item": "Dues and initiation fees",
            "selected": false
        },
        {
            "order": 24,
            "stage": "Automobile/vehicle",
            "item": "Car allowance/reimbursement arrangements if employee using own vehicle",
            "selected": false
        },
        {
            "order": 25,
            "stage": "Automobile/vehicle",
            "item": "Automobile/vehicle assignment/selection if provided with company car",
            "selected": false
        },
        {
            "order": 26,
            "stage": "Automobile/vehicle",
            "item": "License plate ________________________",
            "selected": false
        },
        {
            "order": 27,
            "stage": "Automobile/vehicle",
            "item": "Insurance____________________________",
            "selected": false
        },
        {
            "order": 28,
            "stage": "Automobile/vehicle",
            "item": "Odometer reading _____________________",
            "selected": false
        },
        {
            "order": 29,
            "stage": "Automobile/vehicle",
            "item": "Provide logbook and detailed explanation/illustration of how to complete",
            "selected": false
        },
        {
            "order": 30,
            "stage": "Automobile/vehicle",
            "item": "Parking space assigned to employee if applicable",
            "selected": false
        },
        {
            "order": 31,
            "stage": "Company credit card",
            "item": "Explain and have employee sign off on company’s expense policy/protocol",
            "selected": false
        },
        {
            "order": 32,
            "stage": "Company credit card",
            "item": "Issue company credit card to new hire",
            "selected": false
        },
        {
            "order": 33,
            "stage": "Employee discount card",
            "item": "Explain and have employee sign off on company’s discount policy",
            "selected": false
        },
        {
            "order": 34,
            "stage": "Employee discount card",
            "item": "Issue employee discount card to new hire",
            "selected": false
        },
        {
            "order": 35,
            "stage": "Room &amp; board",
            "item": "Make room &amp; board arrangements or allocate living allowance if applicable",
            "selected": false
        },
        {
            "order": 36,
            "stage": "Travel passes/taxi vouchers",
            "item": "Issue plane/train travel passes/ taxi vouchers if necessary",
            "selected": false
        },
        {
            "order": 37,
            "stage": "Office services",
            "item": "Ergonomic Assessment",
            "selected": false
        },
        {
            "order": 38,
            "stage": "Office services",
            "item": "Additional Furniture required",
            "selected": false
        },
        {
            "order": 39,
            "stage": "Office services",
            "item": "Order business cards",
            "selected": false
        },
        {
            "order": 40,
            "stage": "Office services",
            "item": "Phone list for Help Desk and other key contacts",
            "selected": false
        },
        {
            "order": 41,
            "stage": "Office services",
            "item": "Mail slot assignment",
            "selected": false
        },
        {
            "order": 42,
            "stage": "Office services",
            "item": "Interoffice mail flow",
            "selected": false
        },
        {
            "order": 43,
            "stage": "Office services",
            "item": "Courier instructions",
            "selected": false
        },
        {
            "order": 44,
            "stage": "Office services",
            "item": "Boardroom booking procedures",
            "selected": false
        },
        {
            "order": 45,
            "stage": "Office services",
            "item": "Intranet/library resources",
            "selected": false
        },
        {
            "order": 46,
            "stage": "Office services",
            "item": "Process for ordering supplies",
            "selected": false
        },
        {
            "order": 47,
            "stage": "Office supplies",
            "item": "Issue office supplies for company or home office",
            "selected": false
        },
        {
            "order": 48,
            "stage": "Company information",
            "item": "Provide general company information to new hire including:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Historical context<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Organizational charts<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Key departments/individuals<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Volunteer committees<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Social activities<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Community engagement<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Proximity/nearby services<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Gym, cafeteria/restaurants<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Floor plans and office map",
            "selected": false
        },
        {
            "order": 49,
            "stage": "Safety and evacuation",
            "item": "First aid kits",
            "selected": false
        },
        {
            "order": 50,
            "stage": "Safety and evacuation",
            "item": "Defibrillating machines",
            "selected": false
        },
        {
            "order": 51,
            "stage": "Safety and evacuation",
            "item": "Fire extinguishers",
            "selected": false
        },
        {
            "order": 52,
            "stage": "Safety and evacuation",
            "item": "Fire and emergency marshals",
            "selected": false
        },
        {
            "order": 53,
            "stage": "Policy, education and associated sign-offs",
            "item": "Work safety explanations and employee sign-off",
            "selected": false
        },
        {
            "order": 54,
            "stage": "Policy, education and associated sign-offs",
            "item": "Acceptable use of company resources policy",
            "selected": false
        },
        {
            "order": 55,
            "stage": "Policy, education and associated sign-offs",
            "item": "Waste streams and environmental concerns (eg. for recycling/organic collection and paper reduction – printing double-sided)",
            "selected": false
        },
        {
            "order": 56,
            "stage": "Policy, education and associated sign-offs",
            "item": "Collective Social Responsibility",
            "selected": false
        },
        {
            "order": 57,
            "stage": "Policy, education and associated sign-offs",
            "item": "Employee Conduct",
            "selected": false
        },
        {
            "order": 58,
            "stage": "Policy, education and associated sign-offs",
            "item": "Diversity and inclusion acceptance/sign-off",
            "selected": false
        },
        {
            "order": 59,
            "stage": "Policy, education and associated sign-offs",
            "item": "Harassment Policy",
            "selected": false
        },
        {
            "order": 60,
            "stage": "Policy, education and associated sign-offs",
            "item": "Education policy and resources",
            "selected": false
        },
        {
            "order": 61,
            "stage": "Policy, education and associated sign-offs",
            "item": "Performance evaluations (goals, objectives) and talent management (tuition reimbursement)",
            "selected": false
        },
        {
            "order": 62,
            "stage": "Policy, education and associated sign-offs",
            "item": "Absence guidelines (peak season, holiday closures, blackout periods)",
            "selected": false
        },
        {
            "order": 63,
            "stage": "Policy, education and associated sign-offs",
            "item": "Executive medical exams",
            "selected": false
        },
        {
            "order": 64,
            "stage": "Employee Handbook",
            "item": "Issue employee handbook and offer explanations",
            "selected": false
        },
        {
            "order": 65,
            "stage": "Employee Handbook",
            "item": "Receive employee’s sign-off on employee handbook and policies",
            "selected": false
        },
        {
            "order": 66,
            "stage": "Other",
            "item": "",
            "selected": false
        }
    ],
    "employeeHandbookContents":[
        {
            "order": 2,
            "stage": "Probationary period",
            "item": "Duration, progress review",
            "selected": false
        },
        {
            "order": 3,
            "stage": "Hours of work",
            "item": "Regular hours, overtime, banked lieu time, callin/standby pay, summer hours",
            "selected": false
        },
        {
            "order": 4,
            "stage": "Rest periods and breaks",
            "item": "Entitlement",
            "selected": false
        },
        {
            "order": 5,
            "stage": "Vacation",
            "item": "Entitlement, vacation requests, banking/carrying forward vacation",
            "selected": false
        },
        {
            "order": 6,
            "stage": "Statutory/public holidays",
            "item": "Entitlement, statutory/public holiday pay",
            "selected": false
        },
        {
            "order": 7,
            "stage": "Religious observance",
            "item": "Entitlement, requesting time off",
            "selected": false
        },
        {
            "order": 8,
            "stage": "Floater days",
            "item": "Entitlement, requesting time off",
            "selected": false
        },
        {
            "order": 9,
            "stage": "Sick days",
            "item": "Entitlement, doctor’s notes, unused sick credits",
            "selected": false
        },
        {
            "order": 10,
            "stage": "Personal days",
            "item": "Entitlement, requesting time off",
            "selected": false
        },
        {
            "order": 11,
            "stage": "Bereavement",
            "item": "Entitlement, definitions of recognized relationships",
            "selected": false
        },
        {
            "order": 12,
            "stage": "Professional days",
            "item": "Entitlement, pre-approval",
            "selected": false
        },
        {
            "order": 13,
            "stage": "Jury duty",
            "item": "Explanation of whether salary will continue and whether employee obligated to reimburse any court awarded allowances",
            "selected": false
        },
        {
            "order": 14,
            "stage": "Maternity/paternity/parental/ad option leave",
            "item": "Entitlement, required notice by employer, top-up provisions",
            "selected": false
        },
        {
            "order": 15,
            "stage": "Short/long-term disability",
            "item": "Qualifying criteria, procedures/forms for applying",
            "selected": false
        },
        {
            "order": 16,
            "stage": "Compassionate care leave",
            "item": "Entitlement, required notice by employer",
            "selected": false
        },
        {
            "order": 17,
            "stage": "Reservist leave",
            "item": "Entitlement, required notice by employer",
            "selected": false
        },
        {
            "order": 18,
            "stage": "Group benefits",
            "item": "Entitlement, employee portion of premiums (if applicable)",
            "selected": false
        },
        {
            "order": 19,
            "stage": "Pension/DPSP/RRSP/TFSA",
            "item": "Entitlement, employee contributions (if applicable)",
            "selected": false
        },
        {
            "order": 20,
            "stage": "Taxable benefits/allowances",
            "item": "Entitlement, frequency of reporting/payments",
            "selected": false
        },
        {
            "order": 21,
            "stage": "Bonus",
            "item": "Performance, holiday, discretionary",
            "selected": false
        },
        {
            "order": 22,
            "stage": "Service awards",
            "item": "Entitlements",
            "selected": false
        },
        {
            "order": 23,
            "stage": "Tuition reimbursement",
            "item": "Entitlement, pre-approval, requests for reimbursement",
            "selected": false
        },
        {
            "order": 24,
            "stage": "Deduction from wages",
            "item": "Explanation of deductions: statutory, company compulsory, voluntary",
            "selected": false
        },
        {
            "order": 25,
            "stage": "Expense reimbursement",
            "item": "Pre-approval, requests for reimbursement",
            "selected": false
        },
        {
            "order": 26,
            "stage": "Garnishment against wages",
            "item": "Explanation of employee and employer responsibilities",
            "selected": false
        },
        {
            "order": 27,
            "stage": "Workplace safety/Worker’s compensation",
            "item": "Safety policies, forms, payments",
            "selected": false
        },
        {
            "order": 28,
            "stage": "Internet/company computer policies",
            "item": "Explanation of policies",
            "selected": false
        },
        {
            "order": 29,
            "stage": "Phone/cell phone policies",
            "item": "Explanation of policies",
            "selected": false
        },
        {
            "order": 30,
            "stage": "Travel policies",
            "item": "Travel time policy (meeting at least employment/labour standards)",
            "selected": false
        },
        {
            "order": 31,
            "stage": "Travel policies",
            "item": "Approval Process",
            "selected": false
        },
        {
            "order": 32,
            "stage": "Travel policies",
            "item": "Travel/online booking agent(s)",
            "selected": false
        },
        {
            "order": 33,
            "stage": "Travel policies",
            "item": "Taxi/limo services",
            "selected": false
        },
        {
            "order": 34,
            "stage": "Travel policies",
            "item": "List of preferred hotels/discount codes",
            "selected": false
        },
        {
            "order": 35,
            "stage": "Travel policies",
            "item": "Child/pet/elder/home care options during absences",
            "selected": false
        },
        {
            "order": 36,
            "stage": "Alcohol liability at company events policy",
            "item": "Explanation of policies and sign-off form",
            "selected": false
        },
        {
            "order": 37,
            "stage": "Anti-harassment/bullying policies",
            "item": "Acceptable behaviour with co-workers, management, clients, visitors",
            "selected": false
        },
        {
            "order": 38,
            "stage": "Scent-free policy",
            "item": "Explanation of policy",
            "selected": false
        },
        {
            "order": 39,
            "stage": "Dress code",
            "item": "Explanation of policy",
            "selected": false
        },
        {
            "order": 40,
            "stage": "Privacy/confidentiality",
            "item": "Explanation of policies regarding co-workers, clients/customers, volunteers, public",
            "selected": false
        },
        {
            "order": 41,
            "stage": "Company fraud",
            "item": "Explanation of what may be deemed to be fraud including repercussions",
            "selected": false
        },
        {
            "order": 42,
            "stage": "Termination of employment",
            "item": "Grounds for dismissal",
            "selected": false
        },
        {
            "order": 43,
            "stage": "Termination of employment",
            "item": "Explanation of entitlement to payments upon termination (legislated or company policy if greater)",
            "selected": false
        },
        {
            "order": 44,
            "stage": "Termination of employment",
            "item": "Record of Employment (ROE)",
            "selected": false
        },
        {
            "order": 45,
            "stage": "Appendices",
            "item": "Attendance records template with illustrated example (if applicable)",
            "selected": false
        },
        {
            "order": 46,
            "stage": "Appendices",
            "item": "Time off request forms (vacation, banked lieu time, religious observance, floater/personal days, professional, bereavement, jury duty)",
            "selected": false
        },
        {
            "order": 47,
            "stage": "Appendices",
            "item": "RRSP transfer for retirement allowance/bonus/other",
            "selected": false
        },
        {
            "order": 48,
            "stage": "Appendices",
            "item": "Tuition reimbursement form",
            "selected": false
        },
        {
            "order": 49,
            "stage": "Appendices",
            "item": "Expense reimbursement template with illustrated example",
            "selected": false
        },
        {
            "order": 50,
            "stage": "Appendices",
            "item": "Enrolment/waiver forms for group benefits and company pension plan/DPSP/RRSP/TFSA",
            "selected": false
        },
        {
            "order": 51,
            "stage": "Appendices",
            "item": "Beneficiary allocation sheet with illustrated examples",
            "selected": false
        },
        {
            "order": 52,
            "stage": "Appendices",
            "item": "Benefits claim forms with illustrated examples",
            "selected": false
        },
        {
            "order": 53,
            "stage": "Appendices",
            "item": "Employee sign-off sheets for company policies",
            "selected": false
        },
        {
            "order": 54,
            "stage": "Appendices",
            "item": "Links/paths and explanations to complete all of the above online using the company’s network/Intranet/Internet systems",
            "selected": false
        },
        {
            "order": 55,
            "stage": "Other",
            "item": "",
            "selected": false
        }
    ],
    "integratedPayrollInternet":[
        {
            "order": 2,
            "stage": "Employee handbook",
            "item": "Post entire handbook with searchable hyperlinks",
            "selected": false
        },
        {
            "order": 3,
            "stage": "New Hire/Employee information change forms",
            "item": "Change of address",
            "selected": false
        },
        {
            "order": 4,
            "stage": "New Hire/Employee information change forms",
            "item": "Change banking information",
            "selected": false
        },
        {
            "order": 5,
            "stage": "New Hire/Employee information change forms",
            "item": "Name change (must match SIN card for legal purposes such as T4/RL-1 slips)",
            "selected": false
        },
        {
            "order": 6,
            "stage": "Attendance records",
            "item": "Regular hours, overtime, approved absences",
            "selected": false
        },
        {
            "order": 7,
            "stage": "Requests for time off",
            "item": "Vacation, personal days, floater days/religious observance, banked lieu time, professional",
            "selected": false
        },
        {
            "order": 8,
            "stage": "Self-serve attendance inquiries",
            "item": "Vacation",
            "selected": false
        },
        {
            "order": 9,
            "stage": "Self-serve attendance inquiries",
            "item": "Sick",
            "selected": false
        },
        {
            "order": 10,
            "stage": "Self-serve attendance inquiries",
            "item": "Personal days",
            "selected": false
        },
        {
            "order": 11,
            "stage": "Self-serve attendance inquiries",
            "item": "Banked overtime/lieu time",
            "selected": false
        },
        {
            "order": 12,
            "stage": "Self-serve attendance inquiries",
            "item": "Professional days",
            "selected": false
        },
        {
            "order": 13,
            "stage": "Self-serve attendance inquiries",
            "item": "Summer hours",
            "selected": false
        },
        {
            "order": 14,
            "stage": "TD1/TP-1015.3-V",
            "item": "Electronic TD-1, link to TP-1015.3-V",
            "selected": false
        },
        {
            "order": 15,
            "stage": "CPT30",
            "item": "Link to CPT30 election/revocation form",
            "selected": false
        },
        {
            "order": 16,
            "stage": "RPP/RRSP/TFSA changes",
            "item": "Increase to RPP/RRSP/TFSA contributions (with safeguarded maximums), or requests to decrease amounts",
            "selected": false
        },
        {
            "order": 17,
            "stage": "RRSP transfers",
            "item": "RRSP transfer for retirement allowance/bonus/other (see CPA’s sample Retiring Allowance Transfer Form)",
            "selected": false
        },
        {
            "order": 18,
            "stage": "Automobile/vehicle logbook",
            "item": "Monthly online logbook entries",
            "selected": false
        },
        {
            "order": 19,
            "stage": "Other",
            "item": "",
            "selected": false
        }
    ]
}   

