//language['french'].SelectAll  
export const language = {
    "french": {
        newHireCheckList:"Modèle de liste de vérification des étapes de l'embauche",
        //table titles
        hiringProcess: "Procédure D'Embauche", 
        priorToStartDate: "Avant le Premier Jour D'Emploi", 
        startDateAndBeyond: "Le Premier Jour D'Emploi Et Par La Suite",
        employeeHandbookContents: "Contenu Du Guide De L'Employé",
        integratedPayrollInternet: "Fonctionnalités D'une Solution Intranet Intégrée Pour La Paie",
        //end 
        stage: "Étape",
        action: "Mesure à prendre",
        choice: "Choisir",
        checkAll: "Tout sélectionner",
        uncheckAll: "Tout desélectionner",
        next: "Suivant",
        back: "Retour",
        previous: "Précédent",
        quit: "Quitter",
        restart:"Recommencer",
        print: "Imprimer l'aperçu",
        done: "Terminé",

         //final page
         employeeName: "Nom de l'employé :",
         employeeNumber: "Numéro de l'employé :",
         department: "Department:",
         manager: "Directeur :",
         dateHired: "Date d'embauch :"

    },
    "english":{
        newHireCheckList:"Modèle de liste de vérification des étapes de l'embauche",
         //table titles
        hiringProcess: "Hiring Process", 
        priorToStartDate: "Prior To Start Date",
        startDateAndBeyond: "Start Date and Beyond",
        employeeHandbookContents: "Employee Handbook Contents",
        integratedPayrollInternet: "Integrated Payroll Intranet",
        //end

        stage: "Item",
        action: "Action to be taken",
        choice: "Select",
        checkAll: "Check All",
        uncheckAll: "Unselect All",
        next: "Next",
        back: "Back",
        previous: "Previous",
        quit: "Quit",
        restart:"Restart",
        print: "Print",
        done: "Final",

        //final page
        employeeName: "Employee name:",
        employeeNumber: "Employee number:",
        department: "Service :",
        manager: "Manager:",
        dateHired: "Date hired:"
         
    }
}