import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatStepperModule } from "@angular/material/stepper";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { TableComponent } from './form/table/table.component';
import { StatictableComponent } from './form/statictable/statictable.component'; 

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    TableComponent,
    StatictableComponent,
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule, 
    MatStepperModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
