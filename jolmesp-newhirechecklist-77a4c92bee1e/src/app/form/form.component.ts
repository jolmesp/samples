import { Component, OnInit} from '@angular/core';
import { english, french } from 'src/data/data';
import { language } from 'src/i8n/translations';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
 
  data!: any
  labels: any | null = null


  constructor() { 
  }

  ngOnInit(): void {
    this.setLanguage();
  }
 
  ngDoCheck():void{
    var contentHeight: number = 0;
    if(location.href.indexOf('localhost') == -1){
    
      this.setContainerHeight(0, ".video-container");
      var scrollHeight = window.parent.document.querySelector("iframe")?.contentWindow?.document.body.scrollHeight;
      
      if( scrollHeight){
        contentHeight = scrollHeight;
      
        if(contentHeight){
          contentHeight += 75;
        } 

        this.setContainerHeight(contentHeight, ".video-container");
      }
    }
  }

  setContainerHeight(height:number, containerElement: string){
    var iframeContainer = window.parent.document.querySelector(containerElement);
    window.parent.document.querySelector("iframe")?.setAttribute("style", "width:100%;height:"+ height + "px;");
    iframeContainer?.setAttribute("style", "position:relative;padding-top:0;top: 0;left: 0;width: 100%;margin-top:0;height:" + height + "px;");
  }
 
  setLanguage() {
    if (location.href.indexOf("localhost") != -1 || 
        window.location.href.indexOf('payroll.ca') != -1) {
      this.data = english;
      this.labels = language['english'];
    } else {
      this.data = french;
      this.labels = language['french'];
    }
  }


  saveChoice(eventData: { key: string, index: number, value: boolean }) {
    console.log(eventData);
    this.data[eventData.key][eventData.index].selected = eventData.value;
    console.log(this.data);
  }

  toggleCheck(eventData: { key: string, value: boolean }) {

    this.data[eventData.key].forEach((element: { order: number, stage: string, item: string, selected: boolean }) => {
      element.selected = eventData.value
      console.log(element.selected);
    });
  }
}
