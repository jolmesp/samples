import { Component, Input, OnInit } from '@angular/core';
import { language } from 'src/i8n/translations';

@Component({
  selector: 'app-statictable',
  templateUrl: './statictable.component.html',
  styleUrls: ['./statictable.component.css']
})
export class StatictableComponent implements OnInit {

  @Input() tableData: any;
  @Input() title: string = "";

  filtered: any = [];
  labels: any;
  constructor() {
  }


  ngOnInit(): void {
    if (window.location.href.indexOf('payroll.ca') != -1) {
      this.labels = language['english']
    } else {
      this.labels = language['french']
    }
  }

 

  ngDoCheck(): void {
    this.filtered = this.tableData.filter((i: any) => i.selected == true);
    this.hideDuplicates();
    //console.log('FILTERED', this.filtered);
  }

  hideDuplicates() {
    var rows = document.querySelectorAll('.tableRow');
    var valueToCompareTo = "";

    for (var i = 0; i < rows.length; i++) {

      if (i > 0) {
        var currentColumn = rows[i];

        if (valueToCompareTo == currentColumn.innerHTML) {
          currentColumn.innerHTML = "";
        } else {
          valueToCompareTo = rows[i].innerHTML;
        }
      } else {
        valueToCompareTo = rows[i].innerHTML;
      }
    }
    // console.log(rows);
  }

  previousTitleIsSame(i: number): boolean {
    var previousStageLabel = () => this.filtered[i - 1] == undefined ? null : this.filtered[i - 1].stage;
    var currentStageLabel = this.filtered[i].stage;

    if (i != 0) {
      if (previousStageLabel != undefined && currentStageLabel != undefined) {
        if (previousStageLabel == currentStageLabel) {
          return true;
        }
      }
    }

    return false;
  }

  showStage(i: number): string {
    var previousStageLabel = () => this.filtered[i - 1] == undefined ? null : this.filtered[i - 1].stage;
    var currentStageLabel = this.filtered[i].stage;

    if (i != 0) {
      if (previousStageLabel != undefined && currentStageLabel != undefined) {
        if (previousStageLabel != currentStageLabel) {
          return currentStageLabel;
        }
      }
    }

    return '';
  }

  hasRecords(): boolean {
    //console.log(this.filtered);
    if (this.filtered.length > 0) {
      return true;
    }

    return false;
  }

}
