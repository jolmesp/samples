import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { language } from 'src/i8n/translations';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  
  @Input() title: string = "";
  @Input() tableData: any;
  @Input() isFinal: boolean = false;
  @Input() key!: string;
  @Output() choiceChanged = new EventEmitter<{key: string, index: number, value: boolean}>();
  @Output() toggleCheck = new EventEmitter<{key:string, value: boolean}>();
  
  labels: any;
  buttonLabel!: string; 
  allChecked = false;

  constructor() {
  }

  ngOnInit(): void {
    this.setLanguage();
    this.buttonLabel = this.labels.checkAll;
  }
 
  setLanguage() {
    if (location.href.indexOf("localhost") != -1 || 
        window.location.href.indexOf('payroll.ca') != -1) {
      this.labels = language['english'];
    } else {
      this.labels = language['french'];
    }
  }

  previousTitleIsSame(i: number): boolean {
    if (i != 0) {
      if (this.tableData[i - 1].stage == this.tableData[i].stage) {
        return true;
      }
    }

    return false;
  }



  toggleCheckEvent(){
    this.allChecked = !this.allChecked;
    this.buttonLabel = this.allChecked? this.labels.uncheckAll:this.labels.checkAll;
    this.toggleCheck.emit({key:this.key, value: this.allChecked});
  }

  saveChoiceEvent(i: number, event:any ) {
    if(!this.isFinal){
      this.choiceChanged.emit({key:this.key, index:i, value: event.target.checked});
    }
 
  }

}
