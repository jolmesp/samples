import { Component } from '@angular/core'; 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isLoading = true;
  title = 'NewHireChecklist';

  ngAfterViewInit(){
    this.isLoading = false;
  }

 
}
