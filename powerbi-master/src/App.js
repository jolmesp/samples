import Layout from './components/layout';
import './App.css'; 
import { AuthenticatedTemplate, UnauthenticatedTemplate } from "@azure/msal-react";
import Home from './routes/home'; 
import Login from './routes/login'; 

function App() {
  
  return (
    <>
      <AuthenticatedTemplate>
        <Layout>
              <Home />
        </Layout>
      </AuthenticatedTemplate>
      <UnauthenticatedTemplate>
              <Login />
        
      </UnauthenticatedTemplate>
    </>
  );
}

export default App;
