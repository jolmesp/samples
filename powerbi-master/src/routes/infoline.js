import Layout from "../components/layout";
import PowerBiReport from "../components/report";
import { azureFunctionUrls } from "../config/azureFunctionUrls";

function InfoLine(){
    var currentYear = new Date().getFullYear();
    var previousYear = currentYear - 1;

    return(
        <div>
            <Layout>
                <div className='bi-content'>
                    <h1 className='fs-3 fw-normal my-5'>Infoline Report</h1>
                    <p className='mb-4'>This report details the total number of equiries for the previous and current year {`(${previousYear} to ${currentYear})`} </p> 
                </div>
                <PowerBiReport showPages={false} url={azureFunctionUrls.infoLineLanding} expanded={false} />
            </Layout>
        </div>
    )
}

export default InfoLine;