import PowerBiReport from '../components/report';
import { azureFunctionUrls } from '../config/azureFunctionUrls';

function Home(){
    return(
        <div>
            <div className='bi-content'>
            <h1 className='fs-3 fw-normal my-5'>New Members</h1>
            <p className='mb-4'>This report details new member data for the current year.</p> 
        </div>
        <PowerBiReport showPages={false} url={azureFunctionUrls.membershipReport} expanded={false} />
        </div>
    )
}

export default Home;