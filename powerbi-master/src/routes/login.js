import LogInLogOut from "../components/logInLogOut";

function Login() {
  return (
    <div className="container">
      <section
        className="position-relative mx-auto p-5 m-5"
        style={{ maxWidth: "1024px", outline:'1px solid #ccc' }}>
        <div className="row">
          <div className="col">
            <div className="text-center py-5">
              <img src="/images/npi-logo.png" alt="logo" className="img-fluid d-block mb-5 mx-auto" style={{width:'70%'}} />
              <p className="fs-4">
                  National Payroll Institute Reports
              </p> 
              <LogInLogOut />
            </div>
          </div>

          <div className="col">
            <img
              src="/images/dashboard-illustration.png"
              alt="Dashboard illustration"
              className="img-fluid"
            />
          </div>
        </div> 
      </section>
      <div className="row">
            <div className="col">
                <div className="text-center mt-5 alert">
                    <p className="fs-small pt-3"> 
                        Any issues with the application?  Email <a href="mailto:webmaster@payroll.ca?Subject=Reports website error">webmaster@payroll.ca</a> with a summary & screenshot of the error.
                    </p>
                </div>
            </div>
        </div>
    </div>
  );
}

export default Login;
