import React, { useState } from "react";
import {  useMsal } from "@azure/msal-react";
import { loginRequest } from "../authConfig";
import {callMsGraph} from "../graph";
import { ProfileData } from "./profileData";

function ProfileContent() {
    const { instance, accounts } = useMsal();
    const [graphData, setGraphData] = useState(null);

    const name = accounts[0] && accounts[0].name;

    function RequestProfileData() {
        const request = {
            ...loginRequest,
            account: accounts[0]
        };

        // Silently acquires an access token which is then attached to a request for Microsoft Graph data
        instance.acquireTokenSilent(request).then((response) => {
            callMsGraph(response.accessToken).then(response => setGraphData(response));
        }).catch((e) => {
            instance.acquireTokenPopup(request).then((response) => {
                callMsGraph(response.accessToken).then(response => setGraphData(response));
            });
        });
    }

    return (
        <>
            <p className="text-white text-center fs-small">Logged in as {name}.</p>
            {graphData ? 
                <ProfileData graphData={graphData} />
                :
                ""
            }
        </>
    );
};

export default ProfileContent;