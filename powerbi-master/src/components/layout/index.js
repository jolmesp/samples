import { useIsAuthenticated } from "@azure/msal-react"; 
import Nav from "./nav";
import {useNavigate} from 'react-router-dom';
import ProfileContent from "../profileContent";

function Layout({ children }) {    
    const isAuthenticated = useIsAuthenticated();
    let navigate = useNavigate();

    if(!isAuthenticated){
        navigate('/');
    }

    return (
        <div> 
            <Nav/>
            <section>
                <div className="mx-auto ms-lg-80">
                    <section>
                        <div className='container'>
                            <div className='row'>
                                <div className='col'>
                                    <ProfileContent />
                                    {children}
                                </div>
                            </div>
                        </div>
                    </section> 
                </div>
            </section>
        </div>
    )
}

export default Layout;