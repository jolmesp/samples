import { Link } from "react-router-dom";
import LogInLogOut from "../logInLogOut";
import ProfileContent from "../profileContent"; 

function Nav() {
    return (
        <div> 
            <nav className="navbar d-lg-none navbar-dark bg-dark flex-wrap">
                <div className="container-fluid">
                    <div className="d-flex w-100 align-items-center">
                        <a className="navbar-brand" href="/">
                            <img className="img-fluid" src="/images/NPI-Logo-White.png" alt="" width="auto" style={{width:'50%', height:'auto', margin:'0 auto'}}/>
                        </a>
                        <button className="navbar-burger navbar-toggler bg-primary ms-auto" type="button">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </div>
            </nav>
            <div className="position-relative navbar-menu d-none d-lg-block" style={{ZIndex: 9999}}>
                <div className="navbar-backdrop d-lg-none position-fixed top-0 end-0 bottom-0 start-0  bg-dark" style={{opacity:.5}}></div>
                <div className="position-fixed top-0 start-0 bottom-0 w-nav-max mw-sm-xs pt-6 bg-dark overflow-auto">
                    <div className="px-6 pb-6 position-relative  border-secondary py-3">
                        <div className="d-inline-flex align-items-center">
                          
                            <Link to="/" className="navbar-brand">
                            <img className="img-fluid mx-auto" src="/images/NPI-Logo-White.png" alt="" style={{width:'70%', height:'auto', margin:'0 auto'}}/>
                            </Link>
                        </div>
                    </div>

                    <div className="py-6 px-6">
                        <div>
                            <h3 className="mb-2 text-secondary text-uppercase small">Main</h3>
                            <ul className="nav flex-column mb-8">
                                <li className="nav-item nav-pills">
                                     
                                    <Link to='/infoline' className="nav-link text-white p-3 d-flex align-items-center">
                                    <span className="d-inline-block text-secondary me-4">
                                            <i className="fa-solid fa-phone-volume text-light"></i>
                                        </span>
                                        Infoline</Link>
                                </li> 
                            </ul>  
                            <hr className="text-light"/>
                             <ul className="nav flex-column mb-auto">
                                {/* <li className="nav-item nav-pills">
                                    <a className="nav-link text-white p-3 d-flex align-items-center" href="/">
                                        <span className="d-inline-block text-secondary me-4">
                                            <i className="fa-solid fa-gear"></i>
                                        </span>
                                        <span className="small">Settings</span>
                                    </a>
                                </li> */}
                                 
                               
                                <li className='nav-item nav-pills mx-auto'>
                          
                                    <ProfileContent/>
                                    <p className="text-center"><LogInLogOut /></p>
                                </li>  
                            </ul> 
                        </div>
                    </div>
                </div>
            </div>


        </div>
    )
}

export default Nav;