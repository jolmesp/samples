import { useIsAuthenticated } from "@azure/msal-react";

import { SignInButton } from "./authentication/signInButton";
import { SignOutButton } from "./authentication/signOutButton";

function LogInLogOut(){
    const isAuthenticated = useIsAuthenticated();

    return(
        <>
            { isAuthenticated ? <SignOutButton/> : <SignInButton/> }
        </>
    )
}

export default LogInLogOut;