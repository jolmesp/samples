function Bookmark(){
    
    return( 
        <div className='float-end my-3'>
            <button className='btn btn-warning'>Bookmark this page <i className="fa-solid fa-bookmark"></i></button>
        </div> 
    )
}

export default Bookmark