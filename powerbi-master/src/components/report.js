import React, { useState, useEffect } from 'react';
import { PowerBIEmbed } from 'powerbi-client-react';
import { models } from 'powerbi-client';
import PropTypes from 'prop-types';

function PowerBIReport({url, expanded = false, showPages = false}) {
 
    const [responseConfig, setResponseConfig] = useState({});

    useEffect(() => {
        var options = {
            url: `${url}`, 
            mode:'cors',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
        };
        fetch(options.url, options)
            .then(response => response.json())
            .then(data => {
                //console.log(data);
                setResponseConfig(data);
            })
            .catch(error => {
                console.log(error);
            });

    }, [url]);
    
    return (
        <div>
             <PowerBIEmbed
                embedConfig={{
                    type: "report", // Supported types: report, dashboard, tile, visual and qna
                    id: responseConfig.ReportId,
                    embedUrl: responseConfig.EmbedUrl,
                    accessToken: responseConfig.EmbedToken,
                    tokenType: models.TokenType.Embed,
                    settings: {
                        panes: {
                            filters: {
                                expanded: expanded,
                                visible: true,
                            },
                            pageNavigation: {
                                visible: showPages,
                            },
                        },
                        background: models.BackgroundType.Transparent,
                    },
                }}
                eventHandlers={
                    new Map([
                        [
                            "loaded",
                            function () {
                                console.log("Report loaded");
                            },
                        ],
                        [
                            "rendered",
                            function () {
                                console.log("Report rendered");
                            },
                        ],
                        [
                            "error",
                            function (event) {
                                console.log("ERROR: " + event.detail);
                            },
                        ],
                    ])
                }
                
                cssClassName="report"
            />
        </div>)
}

PowerBIReport.propTypes = {
    url: PropTypes.string.isRequired,
    expanded: PropTypes.bool.isRequired,
    showPages: PropTypes.bool.isRequired
};

export default PowerBIReport;

